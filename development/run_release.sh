#!/bin/sh

currentversion=$(grep 'owl:versionInfo' ../emg.owl | cut -d '>' -f 2 | cut -d '<' -f 1)
mkdir ../previous-versions/$currentversion
mv ../emg.owl ../previous-versions/$currentversion/emg.owl
cp emg-edit.owl ../emg.owl
sed -i "s/>v0</>$1</g" ../emg.owl 
sed -i "s/<owl:versionIRI rdf:resource=\"https:\/\/purls.helmholtz-metadaten.de\/emg\/\"\/>/<owl:versionIRI rdf:resource=\"https:\/\/purls.helmholtz-metadaten.de\/emg\/$1\/\"\/>/g" ../emg.owl 
sed -i "s/emg\/<\/owl:priorVersion>/emg\/$currentversion\/<\/owl:priorVersion>/g" ../emg.owl 
