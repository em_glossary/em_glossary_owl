from owlready2 import *
import types
import yaml
import os
import numpy as np
import glob
import sys

def check_key(indict, keys):
    keys = np.atleast_1d(keys)
    if len(keys) == 1:
        if keys[0] in indict.keys():
            vals = indict[keys[0]]
            if isinstance(vals,list):
                vals.sort()
            return vals
    elif len(keys) == 2:
        if keys[0] in indict.keys():
            if keys[1] in indict[keys[0]].keys():
                vals = indict[keys[0]][keys[1]]
                if isinstance(vals,list):
                    vals.sort()
                return vals
    return []

def capitalize_label(label):
    splitted = label.split()
    capitalized = " ".join([x[0].upper() + x[1:] for x in splitted])
    return capitalized

def convert_string_to_list(vals):
    if isinstance(vals, str):
        vals = [vals]
    return vals

def add_yaml(onto, filename, termdict):
    with open(filename, 'r') as file_in:
        data = yaml.safe_load(file_in)
    onto.base_iri = "https://purls.helmholtz-metadaten.de/emg/"
    classname = os.path.basename(data["iri"])
    with onto:
        NewClass = types.new_class(classname, (Thing,))
    
    vals = convert_string_to_list(check_key(data, ["comments"]))
    for val in vals:
        NewClass.scopeNote.append(val) 
    
    NewClass.label = capitalize_label(check_key(data, ["label"]))
    
    vals = convert_string_to_list(check_key(data, ["acronyms"]))
    for val in vals:
        NewClass.OMO_0003012.append(val)
        
    vals = convert_string_to_list(check_key(data, ["abbreviations"]))
    for val in vals:
        NewClass.OMO_0003000.append(val)
        
    vals = check_key(data, ["contributors"])
    for val in vals:
        NewClass.contributor.append(val)
    
    vals = convert_string_to_list(check_key(data, ["sources"]))
    for val in vals:
        NewClass.IAO_0000119.append(val)
    
    vals = convert_string_to_list(check_key(data, ["synonyms","exact_synonym"]))
    for val in vals:
        NewClass.hasExactSynonym.append(val)
    
    vals = convert_string_to_list(check_key(data, ["synonyms","narrow_synonym"]))
    for val in vals:
        NewClass.hasNarrowSynonym.append(val)
    
    vals = convert_string_to_list(check_key(data, ["synonyms","broad_synonym"]))
    for val in vals:
        NewClass.hasBroadSynonym.append(val)
    
    vals = convert_string_to_list(check_key(data, ["synonyms","related_synonym"]))
    for val in vals:
        NewClass.hasRelatedSynonym.append(val)
    
    NewClass.IAO_0000115 = check_key(data, ["definition"])
    
    vals = convert_string_to_list(check_key(data, ["examples"]))
    for val in vals:
        NewClass.example.append(val)
    
    vals = convert_string_to_list(check_key(data, ["seeAlso"]))
    for val in vals:
        NewClass.seeAlso.append(val)
    
    termdict[NewClass.label[0]] = {}
    termdict[NewClass.label[0]]['classname'] = classname
    termdict[NewClass.label[0]]['subclasses'] = capitalize_label(check_key(data, ["subclassOf"]))
    
def add_contributors(onto):
    contributorlist = []
    for i in onto.classes():
        contributorlist.extend(i.contributor)
    contributorlist = np.unique(contributorlist)
    for i in contributorlist:
        onto.metadata.contributor.append(str(i))
    
def assign_hierarchy(onto, termdict):
    for key,val in termdict.items():
        if val["subclasses"] != "Thing":
            subclass_name = val["classname"]
            if val["subclasses"] in termdict.keys():
                superclass_name = termdict[val["subclasses"]]["classname"]
                getattr(onto, subclass_name).is_a.append(getattr(onto, superclass_name))
                getattr(onto, subclass_name).is_a.remove(owl.Thing)
    
if __name__ == "__main__":
    path = sys.argv[1]
    files = glob.glob(path+'/*.yaml')
    onto = get_ontology("empty.owl").load()
    termdict = {}
    for file in files:
        add_yaml(onto, file, termdict)
    add_contributors(onto)
    assign_hierarchy(onto, termdict)
    onto.save(file = "emg-edit.owl", format = "rdfxml")