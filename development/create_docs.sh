#!/bin/bash

wget -O widoco.jar https://github.com/dgarijo/Widoco/releases/download/v1.4.21/widoco-1.4.21-jar-with-dependencies_JDK-11.jar
java -jar widoco.jar -ontFile ../emg.owl -outFolder public -uniteSections -lang en-de -getOntologyMetadata -noPlaceHolderText -rewriteAll -webVowl
rm -rf ../docs
mkdir ../docs
mv public/doc/* ../docs/
rm -rf public
gawk -i inplace '{gsub("width=\"50/\"","width=\"200/\"",$0); print $0}' ../docs/index-en.html
sed -i 's/Authors/Creators/g' ../docs/index-en.html
sed -i "/The authors/r extra_ack.txt" ../docs/index-en.html 
sed -i "/stylesheet/i\\<link rel=\"icon\" href=\"https:\/\/emglossary.helmholtz-metadaten.de\/favicon.ico\"\>" ../docs/index-en.html
cp ../docs/index-en.html ../docs/index.html
rm widoco.jar

cd ..
git branch -D docs
git push origin -d docs
git checkout --orphan docs
git rm -rf $(ls | grep -v docs)
git add docs
git commit -am 'update documentation'
git push origin docs