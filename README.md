# EM Glossary - *OWL* 
<img src="https://codebase.helmholtz.cloud/em_glossary/em_glossary/-/raw/main/EMGlossary.png" alt="EM Glossary Group Logo" width="400"/> 

# Welcome to the EM Glossary
We work towards harmonized metadata in electron microscopy!

Visit our [webpage](https://emglossary.helmholtz-metadaten.de/) and start developing with us in our [community repository](https://codebase.helmholtz.cloud/em_glossary/em_glossary).  

### In this repository:
This is where the EM Glossary OWL artifact is built, documented, released and maintained. We offer electron microscopy terminology that was harmonized through a community process in a machine-exploitable way. It is intended for integration and adaption into application level semantics and metadata to build bridges between application cases.

In this repository you will find:

| | |
|--|--|
| :page_facing_up: [**emg.owl**](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/emg.owl) | **the current version of the EM Glossary OWL artifact** :exclamation: |
|  | **artifact documentation page can be found [here](https://owl.emglossary.helmholtz-metadaten.de)** |
| :page_facing_up: [ReleaseNotes.md](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/previous-versions/ReleaseNotes.md) | change log of the emg.owl releases |
| :file_folder: development | development files for the OWL artifact such as the automated build pipeline, its requirements and some of its inputs |
| :file_folder: previous versions | previous versions of emg.owl |
| :page_facing_up: [CITATION.cff](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/CITATION.cff) | information on how to cite this repository |
| :page_facing_up: [codemeta.json](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/codemeta.json) | complementary metadata for here deposited files |
| :page_facing_up: [License](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/LICENSE) | copyright and licensing files |


### Further information: 
You have questions :question: Our [**wiki**](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/wikis/home) has the answer :point_up_2:

Find out how:
- we build [emg.owl](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/emg.owl) from the content aggregated in our [community repository](https://codebase.helmholtz.cloud/em_glossary/em_glossary) through an automated pipeline, 
- we handle the release cycle of [emg.owl](https://codebase.helmholtz.cloud/em_glossary/em_glossary_owl/-/blob/main/emg.owl), and
- you can adopt the EM Glossary into your metadata schema or ontology. 
Please also see [artifact documentation page](owl.emglossary.helmholtz-metadaten.de). For more information regarding the EM Glossary initiative and its scope, please visit our [webpage](https://emglossary.helmholtz-metadaten.de/).

### Please cite this repository as
Azocar Guzman, A., Brendike-Mannix, O., Hofmann, V. (2024), Electron Microscopy Glossary, https://purls.helmholtz-metadaten.de/emg/

### Funding
This work was supported by (1) the [Helmholtz Metadata Collaboration (HMC)](www.helmholtz-metadaten.de), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative, specifically HMC Hub Information at the [Institute for Advanced Simulation - Materials Data Science and Informatics (IAS-9)](https://www.fz-juelich.de/de/ias/ias-9) at [Forschungszentrum Jülich GmbH (FZJ)](https://ror.org/02nv7yv05), and HMC Hub Matter at the  Abteilung Experimentsteuerung und Datenerfassung at the [Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB)](https://ror.org/02aj13c28), and (2) the [NFDI-MatWerk](https://nfdi-matwerk.de/) consortium funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under the National Research Data Infrastructure – NFDI 38/1 – project number 460247524.

<p align="center">
  <img alt="Logo_HMC" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/HMC/HMC_Logo_M.png?raw=true" width="300">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_MatWerk" src="https://nfdi-matwerk.de/fileadmin/_processed_/8/3/csm_Logo_NFDI-MatWerk-1000px__1__d04d7a77da.png" width="100">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_FZJ" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/FZJ/Logo_FZ_Juelich_412x120_rgb_jpg.jpg?raw=true" width="200">
&nbsp; &nbsp; &nbsp; &nbsp;
  <img alt="Logo_HZB" src="https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg" width="200">
</p>

### Acknowledgements according to [CRediT](https://credit.niso.org/):
With respect to [EM Glossary project](https://codebase.helmholtz.cloud/em_glossary/), the following individuals are mapped to [CRediT](https://credit.niso.org/) (alphabetical order): \
[Abril Azocar Guzman](https://orcid.org/0000-0001-7564-7990) (AAG); [Mojeeb Rahman Sedeqi](https://orcid.org/0000-0002-9694-0122) (MRS); [Özlem Özkan](https://orcid.org/0000-0003-1965-7996) (ÖÖ); [Oonagh Brendike-Mannix](https://orcid.org/0000-0003-0575-2853)(OBM); [Stefan Sandfeld](https://orcid.org/0000-0001-9560-4728) (StS); [Volker Hofmann](https://orcid.org/0000-0002-5149-603X) (VH); 

Contributions according to CRediT are: \
[Conceptualization](https://credit.niso.org/contributor-roles/conceptualization/): OBM, StS, VH; [Data curation](https://credit.niso.org/contributor-roles/data-curation/): AAG, ÖÖ, OBM, VH; [Funding acquisition](https://credit.niso.org/contributor-roles/funding-acquisition/): OBM, StS, VH; [Methodology](https://credit.niso.org/contributor-roles/methodology/); AAG, MRS, AAG, VH; [Project administration](https://credit.niso.org/contributor-roles/project-administration/): ÖÖ, OBM, VH; [Resources](https://credit.niso.org/contributor-roles/software/): StS, VH; [Software](https://credit.niso.org/contributor-roles/software/): AAG, MRS, VH; [Supervision](https://credit.niso.org/contributor-roles/supervision/): OBM, StS, VH; [Visualization](https://credit.niso.org/contributor-roles/visualization/): AAG, MRS, ÖÖ, OBM, VH;
